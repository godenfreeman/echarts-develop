$(function () {
    $('#crt1').click(function () {
        $(this).hide(1000)
        $('.farm-new').animate({left:"-420"})
        $('#crt2').show(1000)
        $('.drag').show(500,function () {
            $(this).css('background','url(./images/pos-hv.png) no-repeat center center')
        })
        $('.drag').draggable('enable')
    })

    $('#crt2').click(function () {
        $(this).hide(1000)
        $('.farm-set').animate({left:"0"})
        $('.drag').draggable('disable')
        $('.drag').css('background','url(./images/pos-dw.png) no-repeat center center')

    })

    $('.save-new-set').click(function () {
        $('.farm-set').animate({left:"-420"})
        $('.farm-new').animate({left:"0"})
    })

    $('.cancel-new-set').click(function () {
        $('.farm-set').animate({left:"-420"})
        $('.farm-new').animate({left:"0"})
        $('#crt1').show(1000)
        $('.drag').hide(1000)
    })

    $('.op-set').click(function () {
        $('.farm-new').animate({left:"-420"})
        $('#crt1').hide(1000)
        $('#crt2').show(1000)
        $('.drag').show(500,function () {
            $(this).css('background','url(./images/pos-hv.png) no-repeat center center')
        })
        $('.drag').draggable('enable')
    })

});