$(function () {
    $('.panorama').click(function () {
        $('#panoramaShow').toggle(1000)
        $('#videoShow').hide()
        $('#sensorShow').hide()
        $('#agriculturalShow').hide()
        $('#growShow').hide()
    })

    $('.video').click(function () {
        $('#panoramaShow').hide()
        $('#videoShow').toggle(1000)
        $('#sensorShow').hide()
        $('#agriculturalShow').hide()
        $('#growShow').hide()
    })

    $('.sensor').click(function () {
        $('#panoramaShow').hide()
        $('#videoShow').hide()
        $('#sensorShow').toggle(1000)
        $('#agriculturalShow').hide()
        $('#growShow').hide()
    })

    $('.agricultural').click(function () {
        $('#panoramaShow').hide()
        $('#videoShow').hide()
        $('#sensorShow').hide()
        $('#agriculturalShow').toggle(1000)
        $('#growShow').hide()
    })

    $('.grow').click(function () {
        $('#panoramaShow').hide()
        $('#videoShow').hide()
        $('#sensorShow').hide()
        $('#agriculturalShow').hide()
        $('#growShow').toggle(1000)
    })

    $('.x-close').click(function () {
            $(this).parent().parent().hide(1000)
        }
    )

    $('#up-down').click(function () {
        var parent = $(this).parent();
        if(parent.css('height')==='122px'){
            parent.animate({height:"0px"})
            parent.children('.pro-previous').hide()
            parent.children('.pro-next').hide()
            parent.children('.pro-com').hide()
        } else {
            parent.animate({height:"122px"})
            parent.children('.pro-previous').show(1000)
            parent.children('.pro-next').show(1000)
            parent.children('.pro-com').show(1000)
        }
    })

    var proWth = $('.pro-com').width()
    var scrollPic_02 = new ScrollPic();
    scrollPic_02.scrollContId = "isl_cont_1";
    scrollPic_02.arrLeftId = "leftArr";//左箭头ID
    scrollPic_02.arrRightId = "rightArr"; //右箭头ID
    scrollPic_02.frameWidth = proWth;//显示框宽度
    scrollPic_02.pageWidth = 130; //翻页宽度
    scrollPic_02.speed = 10; //移动速度(毫秒，越小越快)
    scrollPic_02.space = 10; //每次移动像素(单位px，越大越快)
    scrollPic_02.autoPlay = false; //自动播放
    scrollPic_02.autoPlayTime = 3; //自动播放间隔时间(秒)
    scrollPic_02.initialize(); //初始化

});