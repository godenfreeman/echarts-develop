$(function () {
    $('.set-base').click(function () {
        $('.farm-base-set').show(1000)
    })

    $('.close-set-farm').click(function () {
        var formSet = $(this).parent().parent().parent().parent()
        formSet.hide(1000)
    })

    $('.save-base').click(function () {
        $('.farm-base-set').hide()
        $('.set-base').hide()
        $('.set-region').show(1000)
    })

    $('.set-region').click(function () {
        $('.farm-region-set').show(1000)
    })

    $('.save-region').click(function () {
        $('.farm-region-set').hide()
        $('.set-field').show(1000)
        $('.farm-field-set').show(1000)
    })
    
    $('.save-field').click(function () {
        $('.farm-field-set').hide()
        $('.set-device').show(1000)
    })
    
    $('.set-device').click(function () {
        $('.farm-device-set').show(1000)
    })
    
    $('.remove-li').click(function () {
        var that = $(this).parent().parent().parent()
        that.remove()
    })

    $('.remove-li-panel').click(function () {
        var that = $(this).parent().parent().parent().parent()
        that.remove()
    })
});