$(function () {
    //初始化比例尺
    $('.scale').html('10 km')

    //切换环境指标
    $('#altitude').click(function () {
        $("#img").attr("src", "../images/altitude.png")
        console.log(2)
    })

    $("#sunshine").click(function () {
        $("#img").attr("src", "../images/sunshine.svg")
        console.log(1)
    })

    $("#frost").click(function () {
        $("#img").attr("src", "../images/frost.svg")
        console.log(1)
    })

    $("#precipitation").click(function () {
        $("#img").attr("src", "../images/precipitation.svg")
        console.log(1)
    })

    $("#temperature").click(function () {
        $("#img").attr("src", "../images/temperature.svg")
        console.log(1)
    })

    $("#humidity").click(function () {
        $("#img").attr("src", "../images/humidity.svg")
        console.log(1)
    })

    //显示
    $('#two1').click(function () {
        $('#con_two_1').toggle(1000)
        $('#con_two_2').hide(1000)
    })

    $('#two2').click(function () {
        $('#con_two_2').toggle(1000)
        $('#con_two_1').hide(1000)
    })

    $('#ser-focus').click(function () {
        $('.search-box').show(1000)
    })

    $('#showFarm').click(function () {
        $('#farm').toggle(1000)
        $('#covers').hide()
    })

    $('#showCovers').click(function () {
        $('#covers').toggle(1000)
        $('#farm').hide()
    })

    $('#doter').click(function () {
        $(this).hide()
        $('.drag').show(1000)
    })

    $('.remove').click(function () {
        $(this).parent().hide(1000)
    })

    $('.drag').draggable({
        containment: ".map-center",
        scroll: false
    });

    $('.drag').mouseup(function () {
        console.log($(this).css('left'))
        console.log($(this).css('top'))
    })

//缩放,带比例尺
    $(document).on("mousewheel DOMMouseScroll", function (e) {

        var delta = (e.originalEvent.wheelDelta && (e.originalEvent.wheelDelta > 0 ? 1 : -1)) || // chrome & ie
            (e.originalEvent.detail && (e.originalEvent.detail > 0 ? -1 : 1)); // firefox

        if (delta > 0) {
            // 向上滚
            //				console.log("wheelup");
            var scale = 10
            scale = scale * 10
            var wit = $('.map-center').width();
            if (wit < 1331) {
                wit += wit * 0.05
                $('.map-center').width(wit)
                $('#img').width(wit)
                $('.scale').html(scale + ' ' + 'km')
            } else {
                $('.map-center').width('1331')
                $('#img').width('1331')
            }

        } else if (delta < 0) {
            // 向下滚
            //				console.log("wheeldown");
            var wit = $('.map-center').width();
            if (wit > 725) {
                wit -= wit * 0.05
                $('.map-center').width(wit)
            } else {
                $('.map-center').width('725')
            }


        }
    });


    //实现地图可拖拽
    $('.map-center').draggable({
        containment: ".map-pos",
        scroll: false
    });
});